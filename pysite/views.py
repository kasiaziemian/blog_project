from django.shortcuts import render, redirect
from django.core.files.storage import FileSystemStorage
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .forms import BookForm
from .models import Book
from django.views.generic import ListView
from django.contrib import messages
from django.http import HttpResponse

def index(request):
        count = User.objects.count()
        return render(request, 'pysite/index.html', {
            'count':count
    })

def book(request):
    context = {}
    if request.method == 'POST':
        uploaded_file = request.FILES['document']
        fs = FileSystemStorage()
        name = fs.save(uploaded_file.name, uploaded_file)
        context['url'] = fs.url(name)
    return render(request, 'pysite/book.html', context)

def book_list(request):
    books = Book.objects.all()
    return render(request, 'pysite/book_list.html', {
        'books': books
    })

def upload_book(request):
    if request.method == 'POST':
        form = BookForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, 'Success!')
            return redirect('book_list')
    else: 
        form = BookForm()
    return render(request, 'pysite/upload_book.html', {
        'form': form
    })
    
def About(request):
    return render(request, 'pysite/About.html')

#def register(request):
 #   if request.method == 'POST':
 #       form = UserCreationForm(request.POST)
 #       if form.is_valid():
 #           user = form.save()
 #           return redirect('Home')
 #   else:
 #       form = UserCreationForm()
 #   return render(request, 'users/register.html', {
 #       'form':form
 #   })

def delete_book(request, pk):
    if request.method == 'POST':
        book = Book.objects.get(pk=pk)
        book.delete()
        messages.warning(request, 'Removed!')
    return redirect('book_list')

def BookListView(ListView):
    model = book
    template_name = 'book_list.html'
    #context_object_name = 'books'



