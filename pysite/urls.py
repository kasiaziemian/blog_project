from django.urls import path, include
from . import views
from users import views as user_view
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_view





urlpatterns = [
    path('', views.index),
    path('Home', views.index, name='Home'),
    path('book', views.book, name='book'),
    path('books/', views.book_list, name='book_list'),
    path('books/upload', views.upload_book, name='upload_book'),
    path('books/<int:pk>/', views.delete_book, name='delete_book'),
    path('About', views.About, name='About'),
    path('register/', user_view.register, name='register'),
    path('profile/', user_view.profile, name='profile'),
    path('login/', auth_view.LoginView.as_view(template_name='users/login.html'), name="login"),
    path('logout/', auth_view.LogoutView.as_view(template_name='users/logout.html'), name="logout"),
    path('accounts/', include('allauth.urls')),
    

    
] 

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

