from django.apps import AppConfig


class PysiteConfig(AppConfig):
    name = 'pysite'
