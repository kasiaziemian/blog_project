FROM python:3.6-alphine

ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /requiremnents.txt

RUN pip install -r / requirements.txt

RUN md /blog
WORKDIR /blog
COPY ./blog /blog

RUN adduser -D user 
USER user 
